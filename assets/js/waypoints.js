// Side Nav in View //

 $(window).scroll(function(){
  // get the height of #wrap
  var h = $('body').height();
  var y = $(window).scrollTop();
  if( y > (h*.075) ){
   // if we are show scrolltotop
   $(".overviewnav").stop().fadeTo('100', 100);
  } else {
   //$("a.scrollup").fadeOut(200);
   $(".overviewnav").stop().hide();
  }
 });

// Waypoints & Nav //

$(window).load(function(){


$.fn.waypoint.defaults = {
  context: window,
  continuous: true,
  enabled: true,
  horizontal: false,
  offset: 90,
  triggerOnce: false
}

    $('#modelfeature a.explore').click(function(){
	    		$('.overviewnav a').removeClass("active");
	    		$('.overviewnav a.overview01').addClass("active");
	    		$('html, body').animate({
					scrollTop: $("#feature01").offset().top-90
				}, 900);
	});

    $('.overviewnav li.overview01').click(function(){
	    		$('.overviewnav a').removeClass("active");
	    		$('.overviewnav a.overview01').addClass("active");
	    		$('html, body').animate({
					scrollTop: $("#feature01").offset().top-90
				}, 900);
	});

	$('.overviewnav li.overview02').click(function(){
	    		$('.overviewnav a').removeClass("active");
	    		$('.overviewnav a.overview02').addClass("active");
	    		$('html, body').animate({
					scrollTop: $("#feature02").offset().top-90
				}, 900);
	});

	$('.overviewnav li.overview03').click(function(){
	    		$('.overviewnav a').removeClass("active");
	    		$('.overviewnav a.overview03').addClass("active");
	    		$('html, body').animate({
					scrollTop: $("#feature03").offset().top-90
				}, 900);
	});

	$('.overviewnav li.overview04').click(function(){
	    		$('.overviewnav a').removeClass("active");
	    		$('.overviewnav a.overview04').addClass("active");
	    		$('html, body').animate({
					scrollTop: $("#feature04").offset().top-90
				}, 900);
	});

	$('.overviewnav li.overview05').click(function(){
	    		$('.overviewnav a').removeClass("active");
	    		$('.overviewnav a.overview05').addClass("active");
	    		$('html, body').animate({
					scrollTop: $("#feature05").offset().top-90
				}, 900);
	});

	$('.overviewnav li.overview06').click(function(){
	    		$('.overviewnav a').removeClass("active");
	    		$('.overviewnav a.overview06').addClass("active");
	    		$('html, body').animate({
					scrollTop: $("#feature06").offset().top-70
				}, 900);
	});

	$('.overviewnav li.overview07').click(function(){
	    		$('.overviewnav a').removeClass("active");
	    		$('.overviewnav a.overview07').addClass("active");
	    		$('html, body').animate({
					scrollTop: $("#feature07").offset().top-70
				}, 900);
	});

	$('.overviewnav li.overview08').click(function(){
	    		$('.overviewnav a').removeClass("active");
	    		$('.overviewnav a.overview08').addClass("active");
	    		$('html, body').animate({
					scrollTop: $("#feature08").offset().top-70
				}, 900);
	});


	/*

	$('#whytopic #whytext .moreinfo a').click(function(){
	    		$('html, body').animate({
					scrollTop: $("#whynewsletter").offset().top-84
				}, 900);
	});

	*/


		/* WAYPOINTS Detection */


   		$('#feature01').waypoint(function() {
			$('.overviewnav a').removeClass("active");
	    		$('.overviewnav a.overview01').addClass("active");
   		}, {offset: 70});

   		$('#feature02').waypoint(function() {
			$('.overviewnav a').removeClass("active");
	    		$('.overviewnav a.overview02').addClass("active");
   		}, {offset: 70});

   		$('#feature03').waypoint(function() {
			$('.overviewnav a').removeClass("active");
	    		$('.overviewnav a.overview03').addClass("active");
   		}, {offset: 70});

   		$('#feature04').waypoint(function() {
			$('.overviewnav a').removeClass("active");
	    		$('.overviewnav a.overview04').addClass("active");
   		}, {offset: 70});

   		$('#feature05').waypoint(function() {
			$('.overviewnav a').removeClass("active");
	    		$('.overviewnav a.overview05').addClass("active");
   		}, {offset: 70});

   		$('#feature06').waypoint(function() {
			$('.overviewnav a').removeClass("active");
	    		$('.overviewnav a.overview06').addClass("active");
   		}, {offset: 70});

   		$('#feature07').waypoint(function() {
			$('.overviewnav a').removeClass("active");
	    		$('.overviewnav a.overview07').addClass("active");
   		}, {offset: 70});

});
