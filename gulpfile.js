var gulp = require('gulp');
var autoprefixer = require('gulp-autoprefixer');
var notify = require('gulp-notify');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
var cssmin = require('gulp-cssmin');
var watch = require('gulp-watch');
var neat = require('node-neat').includePaths;
var uncss = require('gulp-uncss');
var cp = require('child_process');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var newer = require('gulp-newer');

var paths = {
  scss: '_src/scss/**/*.scss',
  css: 'assets/css',
  imgSrc: '_src/img/**',
  imgDest: 'assets/img'
};

gulp.task('jekyll-build', function(done) {
  browserSync.notify('<span style="color: grey">Running:</span> $ jekyll build');
    return cp.spawn('jekyll', ['build'], {stdio: 'inherit'})
      .on('close', done);
});

gulp.task('jekyll-rebuild', ['jekyll-build'], function() {
  browserSync.reload();
});

gulp.task('serve', ['sass', 'jekyll-build'], function() {
  browserSync.init({
    server: {
      baseDir: '_site'
    }
  });
});

gulp.task('sass', function() {
  gulp.src('_src/scss/style.scss')
    .pipe(sass({
      includePaths: ['sass'].concat(neat)
    }))
    .on('error', function(err) {
      console.log(err.message);
      return notify().write(err); // send the error through Growl
    })
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(cssmin())
    .pipe(gulp.dest('_site/assets/css/'))
    .pipe(browserSync.reload({stream:true}))
    .pipe(gulp.dest('./assets/css/'))
    .pipe(notify('Sass compiled successfully.'));
});

gulp.task('imagemin', function() {
  gulp.src(paths.imgSrc)
    .pipe(newer(paths.imgDest))
    .pipe(imagemin({
      progressive: true,
      svgoPlugins: [{removeViewBox: false}],
      use: [pngquant()]
    }))
    .pipe(gulp.dest(paths.imgDest));
});

gulp.task('uncss', function() {
  gulp.src('assets/css/style.css')
    .pipe(uncss({
      html: ['*.html','btrain/**/*.html','dealers/*.html','downloads/*.html','dropdeck/**/*.html','flatdeck/**/*.html','grain/**/*.html','products/*.html','specialized/**/*.html','company/**/*.html']
    }))
    .pipe(gulp.dest('./out/root'));
});

gulp.task('watch', function() {
  gulp.watch(paths.scss, ['sass']);
  gulp.watch(paths.imgSrc, ['imagemin']);
  gulp.watch(['*.html', '_layouts/*.html', '_includes/*.html', 'grain/**/*.html'], ['jekyll-rebuild']);
});

gulp.task('default', ['serve', 'sass', 'imagemin', 'watch']);
