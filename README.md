# LK Example

## Getting Started

1. Clone this repository to your machine.
2. Open the directory in your Terminal, run `npm install`.
3. Run `gulp`. This will compile the Jekyll site and open up a new Browsersync window.
